## **About**
This task is based on using arrays and selecting random elements from arrays. 
 
In a pack of playing cards there are 4 suits:  
 
- clubs 
- diamonds 
- hearts 
- spades 
 
There are 13 cards in each suit:  
 
- Ace 
- 2 
- 3 
- 4 
- 5 
- 6 
- 7 
- 8 
- 9 
- 10 
- Jack 
- Queen 
- King 

</br>

## **Task**
Your task is to create two arrays, one for the suits and one for the values. 
 
You then need to select a random element from each array which will then represent a playing card. 
 
**Example**

For example, if a ​spade​ was randomly selected from the suits array and a ​5​ from the values array then 
the playing card would be the ​5 of Spades​. 
 
You simply need to output the playing card to the user. 
 
E.g. The randomly selected card is the 5 of spades. 
