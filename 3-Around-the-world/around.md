## **About**

This tasks has been designed to provide you with an opportunity to work with arrays.  You will practise 
sorting arrays and adding values to an existing array. 
 
It is common to have to work with arrays and array values, especially when dealing with database data. 
Therefore, the more comfortable you are using arrays the better. 
 
 
## **Task**
 
Place the following cities into an array: 
 
London, Paris, Amsterdam, New York, Berlin, Brisbane 
 
Sort the cities alphabetically. 
 
Display the cities as an ordered list.  Your output will be like this: 
 
1. Amsterdam 
2. Berlin 
3. Brisbane 
4. London 
5. New York 
6. Paris 
 
Now add the following cities by using the array_push function: 
 
Sydney, Helsinki, Beijing, Dublin, Rome 
 
Sort the cities alphabetically. 
 
Display the cities as an ordered list. 
 
 
 
</br></br>
 
## **HINTS AND TIPS**
 
sort() 
 
foreach (){} 
 
array_push() 
 