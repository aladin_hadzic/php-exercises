## **About**
Each book published is given a unique identifier called an 
International Standard Book Number or ISBN for short.   
 
The ISBN is 13 characters long if assigned from 1 January 
2007 and 10 characters long if assigned before that date. 
 
This task has been designed to test your ability to create a 
form and process the user input.

## **Task**
Design a HTML form to allow a user to input a ​10 digit ​ISBN. 
 
Send this data to a processing script and then check if it is a valid ISBN number. 
 
Output a valid or invalid message to the user. 
 
Sample valid ISBN: ​1573980137
</br></br>

**HOW TO CHECK IF AN ISBN IS VALID (Length of 10)**

- STEP 1: Multiply each digit of the ISBN by a weighted value.   
The first digit is multiplied by 1, the second digit by 2, the third digit by 3 etc. 
 
- STEP 2: Find the total of these multiplied digits. 
 
- STEP 3: If the total is divided by 11 and there is no remainder then the ISBN is valid. 
</br></br>

**Example**

Consider the ISBN: 0072296542

STEP1: 

0 x 1  = 0

0 x 2  = 0 

7 x 3  = 21 

2 x 4  = 8 

2 x 5  = 10 

9 x 6 = 54 

6 x 7 = 42 

5 x 8 = 40 

4 x 9 = 36 

2 x 10 = 20 
 
STEP 2: 

0 + 0 + 21 + 8 + 10 + 54 + 42 + 40 + 36 + 20 = 231 
 
STEP 3: 

231 / 11 = 21  (i.e. no remainder) 
 
*Therefore the ISBN is valid*

</br></br>

## **Hints and tips**

You may wish to check for spaces and/or use of the dash and replace these prior to performing the 
calculation. 
 
You can check the string length in the form input field or use strlen() when processing. 
 
Convert the user input string into an array using str_split(). 
 
Loop through each number of the array and process. 
 
Use modulus to check if the total is divisible by 11 and leaves a remainder