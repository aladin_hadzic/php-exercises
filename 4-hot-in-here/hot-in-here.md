## **About**

The temperatures in degrees C were recorded in January 2015 as follows: 
 
32.3, 31.3, 28.2, 29.3, 29.7, 29.9, 28.7, 28.4, 30.5, 30.5, 31.7, 30.6, 29.4, 32.0, 
36.2, 31.3, 32.8, 33.3, 32.9, 28.8, 30.8, 28.0, 25.9, 30.8, 32.4, 32.0, 31.3, 25.2, 
29.1, 28.6, 30.6 
 
Write a script to output the following: 
 
- average daily temperature in degrees C  (rounded to 1 decimal place)

- the 5 lowest temperatures 

- the 5 highest temperatures 
 
 
Your output should look something like this: 
 
- The average daily temperature is 30.4°C 

- The 5 lowest temperatures are: 25.2°C 25.9°C 28°C 28.2°C 28.4°C  

- The 5 highest temperatures are: 32.4°C 32.8°C 32.9°C 33.3°C 36.2°C 
 
 
Create an array and store all of your values in the array.  You can copy and paste the value above to 
save you some time. 
 
 
## **HINTS AND TIPS**
 
**array_sum()** - Find the total of the values in the array 
 
**count()** - Count the number of values in the array 
 
 
Once you know the sum and count you can calculate the average using the 
following formula: 
 
**average = sum / count**
 
 
**array_slice()** Useful for finding the highest and lowest temperatures 
 
**round()** For rounding numbers 