## **About**

When accepting user inputs and posting to live environments, such as forums or comments boxes, we 
need to take into account any inappropriate words the user may use and we may wish to remove or 
replace these. 
 
This task has been designed to provide you with an opportunity to accept a user input and then filter this 
input and replace any inappropriate words. 
 
Note that this is a basic introduction to the filtering process and much more comprehensive processes 
would need to be employed on a live environment.

## **Task**

Create a HTML form which allows the user to input a string of text. 
 
Send this text to a processing script that will search through the text and replace any inappropriate words 
with ****

**Example**

The user may input: 
 
I love this website but feel that the stupid ​badword1​ comments are detracting from the user 
experience.  The person who wrote this is a ​badword2​. 
 
where badword1 and badword2 would be something inappropriate. So as not to offend anyone we shall 
just use badword1 and badword2. 
 
*The output from this sample would be:*
 
I love this website but feel that the stupid ​****​ comments are detracting from the user experience. 
The person who wrote this is a ****.

</br>

## **Hints and tips**

Store any bad words you want to check in an array and then loop through the array.  In the ‘do 
something’ part of the loop, use str_replace() to replace each of the bad words with ****